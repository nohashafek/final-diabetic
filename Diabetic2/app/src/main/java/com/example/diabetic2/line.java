package com.example.diabetic2;

public class line {
    long xValue;
    int yVlue;

    public line() {
    }

    public line(long xValue, int yVlue) {
        this.xValue = xValue;
        this.yVlue = yVlue;
    }

    public long getxValue() {
        return xValue;
    }

    public int getyVlue() {
        return yVlue;
    }
}
