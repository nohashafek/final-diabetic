package com.example.diabetic2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ProgrammingKnowledge on 4/3/2015.
 */
public class
DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "userprofile.db";
    public static final String TABLE_NAME = "userprofile_table";
   // public static final String COL_1 = "ID";
    public static final String COL_1 = "email";
    public static final String COL_2= "username";
    public static final String COL_3= "password";
    public static final String COL_4 = "phone";
    public static final String COL_5 = "birthday";
//    public static final String COL_6= "gender_type";
//    public static final String COL_7 = "diabetes_type";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME +" (email TEXT PRIMARY KEY ,username TEXT,password TEXT,phone TEXT,birthday TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(String email,String username,String password,String phone,String birthday) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,email);

        contentValues.put(COL_2,username);
        contentValues.put(COL_3,password);
        contentValues.put(COL_4,phone);
        contentValues.put(COL_5,birthday);
//        contentValues.put(COL_6,gender_type);
//        contentValues.put(COL_7,diabetes_type);
        long result = db.insert(TABLE_NAME,null ,contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }

    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_NAME,null);
        return res;
    }

    public boolean updateData(String email,String username,String password,String phone,String birthday) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,email);

        contentValues.put(COL_2,username);
        contentValues.put(COL_3,password);
        contentValues.put(COL_4,phone);
        contentValues.put(COL_5,birthday);
//        contentValues.put(COL_6,gender_type);
//        contentValues.put(COL_7,diabetes_type);
        db.update(TABLE_NAME, contentValues, "email = ?",new String[] {email });
        return true;
    }

    public Integer deleteData (String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "email = ?",new String[] {email});
    }
}
