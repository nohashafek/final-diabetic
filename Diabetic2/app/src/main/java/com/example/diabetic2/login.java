package com.example.diabetic2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.net.PasswordAuthentication;

public class login extends AppCompatActivity {

    EditText emaill,pass;
    FirebaseAuth fAuth;
    Button log ;
    TextView forget,register;
    ProgressBar progressBar;
    AlertDialog.Builder builder;
    CheckBox show;
    RadioButton remmber;
    public String FirstTime;

    private long backpresstime;
    public static  String idd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emaill=findViewById(R.id.user);
        pass=findViewById(R.id.password);
        log=findViewById(R.id.login);
        register=findViewById(R.id.createnewaccount);
        forget=findViewById(R.id.forgetpass);
        progressBar=findViewById(R.id.progressbar);
        remmber=findViewById(R.id.radio);
        show=findViewById(R.id.showpass);

        fAuth=FirebaseAuth.getInstance();




        SharedPreferences preferences=getSharedPreferences("checkbox",MODE_PRIVATE);

        FirstTime=preferences.getString("remember","false");

        if (FirstTime.equals("true")  ){
            FirebaseUser user = fAuth.getCurrentUser();
            idd=user.getUid();
            startActivity(new Intent(getApplicationContext(),choose.class));
        }

        show.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    pass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else {
                    pass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });



        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),createaccount.class));
            }
        });





        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email=emaill.getText().toString().trim();
                String password=pass.getText().toString().trim();


                if(TextUtils.isEmpty(email)){
                    emaill.setError("email is required");
                    return;
                }
                if(TextUtils.isEmpty(password)){
                    pass.setError("password is required");
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);
                fAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            SharedPreferences preferences=getSharedPreferences("checkbox",MODE_PRIVATE);

                            SharedPreferences.Editor editor=preferences.edit();
                            editor.putString("remember","true");
                            editor.apply();
                            Toast.makeText(login.this,"Logged in successfully",Toast.LENGTH_LONG).show();
                            //check
                            FirebaseUser user = fAuth.getCurrentUser();
                            idd=user.getUid();


//                            updateUI(user);
                            startActivity(new Intent(getApplicationContext(),choose.class));

                        }else {
                            Toast.makeText(login.this,"incorrect email or password",Toast.LENGTH_LONG).show();
                            progressBar.setVisibility(View.VISIBLE);
                        }

                    }
                });

            }
        });



        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final EditText resetmail = new EditText(v.getContext());
                AlertDialog.Builder PasswordResetDialog = new AlertDialog.Builder(v.getContext());
                PasswordResetDialog.setTitle("Reset Password ?");
                PasswordResetDialog.setMessage("Please Enter Your Email To Receiver Reset Link.");
                PasswordResetDialog.setView(resetmail);

                PasswordResetDialog.setPositiveButton("SEND RESET EMAIL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String mail= resetmail.getText().toString().trim();
                        fAuth.sendPasswordResetEmail(mail).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(getApplicationContext(),"please check your email",Toast.LENGTH_SHORT).show();

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getApplicationContext(),"Error ! Reset Link is Not Sent " + e.getMessage(),Toast.LENGTH_SHORT).show();

                            }
                        });


                    }
                });
                PasswordResetDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                PasswordResetDialog.create().show();

            }

        });

    }


    public void onBackPressed() {

        if(backpresstime + 2000 > System.currentTimeMillis()){
            Intent exit=new Intent(Intent.ACTION_MAIN);
            exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            exit.addCategory(Intent.CATEGORY_HOME);
            startActivity(exit);
            finish();
            System.exit(0);
            return;
        }else{
            Toast.makeText(getBaseContext(),"press back again to exit",Toast.LENGTH_SHORT).show();
        }
        backpresstime=System.currentTimeMillis();
    }

}