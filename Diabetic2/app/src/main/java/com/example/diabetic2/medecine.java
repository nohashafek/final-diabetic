package com.example.diabetic2;

public class medecine {
    String name, concentration,category;

    public medecine(String name, String concentration,String category) {
        this.name = name;
        this.concentration = concentration;
        this.category=category;


    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConcentration() {
        return concentration;
    }

    public void setConcentration(String concentration) {
        this.concentration = concentration;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


}
