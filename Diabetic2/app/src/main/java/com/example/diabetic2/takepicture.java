package com.example.diabetic2;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class takepicture extends AppCompatActivity {

    private static final int PICK_IMAGE_REQUEST = 1;
    private Button mButtonChooseImage;
    private Button mButtonUpload;
    private ImageView mImageView;
    private ProgressBar mProgressBar;
    private Uri mImageUri;
    private StorageReference mStorageRef;
    private DatabaseReference meal,idpatient,fooo,cal,gm;
    FirebaseDatabase mFirebaseDatabase;
    private StorageTask mUploadTask;
    TextView txt,gmtext,caloriestext;
    String name;
    String nameofp;
    int i=0;
    String id;
    DatabaseAccess databaseAccess;
    String quotes[];
    public static String radio_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_takepicture);
        databaseAccess = DatabaseAccess.getInstance(this);
        id=login.idd;

        mImageView= findViewById(R.id.image_view);
//        cameraBtn = findViewById(R.id.capture);
        txt = findViewById(R.id.ing1);
        mButtonChooseImage = findViewById(R.id.cchoosefile);
        mButtonUpload = findViewById(R.id.uploadimage);
        mProgressBar = findViewById(R.id.progress_bar);
        gmtext=findViewById(R.id.gm1);
        caloriestext=findViewById(R.id.cal1);

        mStorageRef = FirebaseStorage.getInstance().getReference("uploads");
        mFirebaseDatabase = FirebaseDatabase.getInstance();

        meal=mFirebaseDatabase.getReference("food").child("meal");
        cal=mFirebaseDatabase.getReference("calo");
        gm=mFirebaseDatabase.getReference("calo2");
        idpatient=mFirebaseDatabase.getReference("Patients").child(id);
        fooo=mFirebaseDatabase.getReference("food").child("id");



        mButtonChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                meal.setValue(" ");
                openFileChooser();
            }
        });


        mButtonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUploadTask != null && mUploadTask.isInProgress()) {
                    Toast.makeText(takepicture.this, "Upload in progress", Toast.LENGTH_SHORT).show();

                } else {
                    uploadFile();


                }
            }
        });



    }
    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            mImageUri = data.getData();
            Picasso.with(this).load(mImageUri).into(mImageView);
        }
    }

    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void uploadFile() {
        if (mImageUri != null) {
            StorageReference fileReference = mStorageRef.child("akel"
                    + "." + getFileExtension(mImageUri));
            mUploadTask = fileReference.putFile(mImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mProgressBar.setProgress(0);
                                }
                            }, 500);
                            Toast.makeText(takepicture.this, "Upload successful", Toast.LENGTH_LONG).show();
                            idpatient.child("Image").setValue("akel"
                                    + "." + getFileExtension(mImageUri));
                            fooo.setValue(id);

                            Toast.makeText(takepicture.this, "Please wait a moment", Toast.LENGTH_LONG).show();

//                            Upload upload = new Upload("na",
//                                    taskSnapshot.getStorage().getDownloadUrl().toString());
//                            String uploadId = mDatabaseRef.push().getKey();
//                            mDatabaseRef.child(uploadId).setValue(upload);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(takepicture.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            mProgressBar.setProgress((int) progress);
                        }
                    });
        } else {
            Toast.makeText(this, "No file selected", Toast.LENGTH_SHORT).show();
        }
    }


    protected void onStart() {
        super.onStart();
        meal.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                name = dataSnapshot.getValue(String.class);
                txt.setText(name);
                databaseAccess.open();
        quotes = new String[2];
        radio_text="per 100 gm";

        quotes = databaseAccess.getQuotes(name,radio_text);
        databaseAccess.close();
        caloriestext.setText(quotes[0]);
        gmtext.setText(quotes[1]);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        idpatient.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds:dataSnapshot.getChildren())
                {
                    nameofp=""+ ds.child("profile").child("id").getValue();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


//    @Override
//    protected void onStart() {
//        super.onStart();
//        databaseAccess1.open();
//        quotes1 = new String[2];
//        radio_text1="per 100 gm";
//         String givenmeal="apple" ;
//        quotes1 = databaseAccess1.getQuotes(givenmeal,radio_text1);
//        databaseAccess1.close();
//        caloriestext.setText(quotes1[0]);
//        gmtext.setText(quotes1[1]);
//        txt.setText(givenmeal);
//
//    }
}

