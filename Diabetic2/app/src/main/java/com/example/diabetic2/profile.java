package com.example.diabetic2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayList;

public class profile extends AppCompatActivity {
    TextView ssname,ssphone,ssmail,ssbirth,ssgender,sstype,ssid;
   FirebaseDatabase mFirebaseDatabase;
    FirebaseAuth mAuth;
    FirebaseUser user;
   DatabaseReference myRef;
     String userID;
     String em;
    private static final String USERS = "Patients";
    String main;
    Button editpro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ssname=findViewById(R.id.proname);
        ssphone=findViewById(R.id.prophone);
        ssmail=findViewById(R.id.promail);
        ssbirth=findViewById(R.id.probirth);
        ssgender=findViewById(R.id.progender);
        sstype=findViewById(R.id.protype);
        ssid=findViewById(R.id.proid);
        editpro=findViewById(R.id.edit);


        main=login.idd;

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference(USERS).child(main);

        editpro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),editprofile.class));
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds:dataSnapshot.getChildren())
                {

                    String name=""+ ds.child("username").getValue();
                    String mail=""+ ds.child("mail").getValue();
                    String phone=""+ ds.child("phone").getValue();
                    String age=""+ ds.child("birth").getValue();
                    String type=""+ ds.child("typew").getValue();
                    String gender=""+ ds.child("gender").getValue();
                    String id = "" + ds.child("id").getValue();

                    ssname.setText(name);
                    ssphone.setText(phone);
                    ssbirth.setText(age);
                    ssmail.setText(mail);
                    ssgender.setText(gender);
                    sstype.setText(type);
                    ssid.setText(id);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



}