package com.example.diabetic2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

public class laboratory extends AppCompatActivity {

    Button suradd;
    TextView tsug,theart,tbloodpre,ttemp;

    FirebaseDatabase database;
    DatabaseReference reference;
    private static final String USERS = "Patients";
    String main;

    SimpleDateFormat sdf=new SimpleDateFormat("Mdh");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laboratory);

        main=login.idd;
        database = FirebaseDatabase.getInstance();
        reference = database.getReference(USERS).child(main);

        suradd=findViewById(R.id.sugaradd);
        tsug=findViewById(R.id.sugar);
        theart=findViewById(R.id.heartratee);
        ttemp=findViewById(R.id.tempra);
        tbloodpre=findViewById(R.id.blopre);

        suradd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id=reference.push().getKey();
                int yn= Integer.parseInt(tsug.getText().toString());
                long xn=new Date().getTime();
                pointline point=new pointline(xn,yn);

                String blood=tbloodpre.getText().toString().trim();
                String glu=tsug.getText().toString().trim();
                String hear=theart.getText().toString().trim();
                String te=ttemp.getText().toString().trim();

                reference.child("medicalinformation").child("glucosse").child(id).setValue(point);
                reference.child("medicalinformation").child("lastglucose").setValue(glu);
                reference.child("profile").child("lastglucose").setValue(glu);
                reference.child("medicalinformation").child("bloodpresure").setValue(blood);
                reference.child("medicalinformation").child("heartrate").setValue(hear);
                reference.child("medicalinformation").child("temp").setValue(te);
                Toast.makeText(laboratory.this,"Data has been uploaded",Toast.LENGTH_LONG).show();

                tbloodpre.setText("");
                theart.setText("");
                ttemp.setText("");
               tsug.setText("");
            }
        });


    }
}
