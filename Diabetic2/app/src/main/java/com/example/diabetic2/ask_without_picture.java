package com.example.diabetic2;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ask_without_picture extends AppCompatActivity {
    private Spinner ingredient;
    private String[] meals = {"spaghetti","mozarella","peas","white beans","beef","chicken","chicken liver","french fries","apple juice","lemon juice","orange juice","mango juice","birthday cake","feta","salami","chocolate bar","apple","banana","grapes","mango","orange","strawberry","watermelon","sandwich bread"};
    public static String returnedmeal="hi";
    TextView caloriestext;
    TextView ing_text;
    TextView gm_text;
    TextView carbs_text;

    DatabaseAccess databaseAccess;
    String quotes[];
    RadioGroup grp;
    RadioButton optionselected;
   public static String radio_text;
   Button showcalories;
    String back;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_without_picture);
        databaseAccess = DatabaseAccess.getInstance(this);
        caloriestext=findViewById(R.id.cal);
        ing_text=findViewById(R.id.ing);
        gm_text=findViewById(R.id.gm);
        //carbs_text=findViewById(R.id.carbs);
        //showcalories=findViewById(R.id.showcalories);

     addItemsOnSpinner();
        Log.d("come3", "This "+returnedmeal);






//        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
//        //databaseAccess.open();
//      String quotes = databaseAccess.getQuotes();
//        //databaseAccess.close();
//     caloriestext.setText(quotes);

    }



    public  void addItemsOnSpinner(){
//
       // grp.clearCheck();

     //radio_text="per 100 gm";
        ingredient=findViewById(R.id.ingredient);
        ingredient.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, meals));
        // returnedmeal=ingredient.getSelectedItem().toString();
        // Toast.makeText(ask_without_picture.this, ""+returnedmeal, Toast.LENGTH_SHORT).show();
        ingredient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            returnedmeal=parent.getItemAtPosition(position).toString();
                //Toast.makeText(ask_without_picture.this, ""+returnedmeal, Toast.LENGTH_SHORT).show();

                Log.d("come", "This "+returnedmeal);




            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Log.d("come2", "This "+returnedmeal);
        //return  returnedmeal;


    }


    public void select1(View view) {

   Log.d("come4", "This "+returnedmeal);
        grp=findViewById(R.id.grp);
        int selectedId = grp.getCheckedRadioButtonId();
        Log.d("radio", "This "+selectedId);

        optionselected=findViewById(selectedId);

       radio_text="per 100 gm";
      // radio_text=optionselected.getText().toString();



    }

    public void select2(View view) {

        Log.d("come4", "This "+returnedmeal);
        grp=findViewById(R.id.grp);
        int selectedId = grp.getCheckedRadioButtonId();
        Log.d("radio", "This "+selectedId);

        optionselected=findViewById(selectedId);

        radio_text="per serving";
        // radio_text=optionselected.getText().toString();



    }

    public void show(View view) {
        Log.d("come5", "This "+radio_text);
        databaseAccess.open();
        quotes = new String[2];
        quotes = databaseAccess.getQuotes(returnedmeal,radio_text);
        databaseAccess.close();
        caloriestext.setText(quotes[0]);
        gm_text.setText(quotes[1]);
        ing_text.setText(returnedmeal);
    }
}
