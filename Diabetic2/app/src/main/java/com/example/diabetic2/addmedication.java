package com.example.diabetic2;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;


public class addmedication extends AppCompatActivity {
    FirebaseDatabase mFirebaseDatabase;
    FirebaseAuth mAuth;
    FirebaseUser user;
    DatabaseReference myRef;
    DatabaseReference bRef1;
    String patientid;
    String id1;
    boolean first = true;
    private String[] meals = {"Select category","before breakfast","after breakfast","before lunch","after lunch","before dinner","after dinner","before exercise","after exercise"};
    private Spinner category;
    ArrayList<String> transmit =new ArrayList<String>();


    DatePickerDialog dp;
    int year;
    int month;
    int day;
    Calendar cal;
    Button reminderr;
   public EditText name1;
   public EditText gm1;
    Button selecttime;
   public String returnedcategory;
   public static String  medicationname;
    public static String  transmitted;
    String medicationgm;
int counter=0;
medecine m;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addmedication);
        mAuth=FirebaseAuth.getInstance();

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference("Patients");
        user=mAuth.getCurrentUser();
//        patientid= user.getUid();// bigbli id tany
      patientid=login.idd;
//        myRef.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                for (DataSnapshot ds:dataSnapshot.getChildren())
//                {
//               patientid=""+ ds.getKey();
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
       bRef1= myRef.child( patientid).child("prescriptions");


        category = findViewById(R.id.category);
        reminderr=findViewById(R.id.reminder);
    name1=findViewById(R.id.name);
  gm1=findViewById(R.id.gm);
  selecttime=findViewById(R.id.time);

        category.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, meals));
        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                for(int i=0; i<=position;i++) {
                    returnedcategory=meals[i];
//                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(addmedication.this);
//                    SharedPreferences.Editor editor = prefs.edit();
//                    editor.putString("string_id", returnedcategory); //InputString: from the EditText
//                    editor.commit();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        TextView text= findViewById(R.id.tdate);
//        Button button=findViewById(R.id.bdate);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                cal=Calendar.getInstance();
//                year=cal.get(Calendar.YEAR);
//                month=cal.get(Calendar.MONTH);
//                day=cal.get(Calendar.DAY_OF_MONTH);
//
//
//
//
//
//
//
//
//            }
//        });

       reminderr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               medicationname=name1.getText().toString();
                 medicationgm=gm1.getText().toString();
                id1 = bRef1.push().getKey();
                m=new medecine(medicationname,medicationgm,returnedcategory);
                bRef1.child(id1).setValue(m);

                Intent back=new Intent(addmedication.this, medication.class);
               back.putExtra("n",medicationname);
                back.putExtra("g",medicationgm);
                back.putExtra("c",returnedcategory);

//                SharedPreferences prefs1 = PreferenceManager.getDefaultSharedPreferences(addmedication.this);
//                SharedPreferences.Editor editor1 = prefs1.edit();
//                editor1.putString("string_id1", medicationname); //InputString: from the EditText
//                editor1.commit();
                setResult(RESULT_OK, back);
                finish();
            }
        });

       selecttime.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
             medicationname=name1.getText().toString();
             transmit.add(counter,medicationname);
            transmitted=transmit.get(counter);
              counter ++;
               Log.d("transmit", "This "+ medicationname);
               medicationgm=gm1.getText().toString();
               SharedPreferences prefs1 = PreferenceManager.getDefaultSharedPreferences(addmedication.this);
               SharedPreferences.Editor editor1 = prefs1.edit();
               editor1.putString("string_id1", medicationname); //InputString: from the EditText
               editor1.commit();
               Intent i=new Intent(addmedication.this, alarm.class);
               alarm.broadcastCode++;

               startActivity(i);
           }
       });

    }
}
