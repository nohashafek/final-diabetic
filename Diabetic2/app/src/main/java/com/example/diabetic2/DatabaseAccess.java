package com.example.diabetic2;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseAccess{
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;
    String returned_meal;
    String getcalories;
   String getgm;


    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Read all quotes from the database.
     *
     * @return a List of quotes
     */
    public String[] getQuotes(String returned_meal,String option) {
        //List<String> list = new ArrayList<>();

//returned_meal=ask_without_picture.returnedmeal;
        Log.d("tagg", "This is my message"+ returned_meal);
        if(option == "per 100 gm"){
            Cursor cursor = database.rawQuery("Select calfor100gm From caloriescalculator Where name = '" + returned_meal + "'", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                getcalories=String.valueOf(cursor.getFloat(0));
                // String gett=returned_meal;
                cursor.moveToNext();
            }
            cursor.close();
          getgm="100";
        }
        else if(option == "per serving"){
            Cursor cursor = database.rawQuery("Select calforserving From caloriescalculator Where name = '" + returned_meal + "'", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                getcalories=String.valueOf(cursor.getFloat(0));
                // String gett=returned_meal;
                cursor.moveToNext();
            }
            cursor.close();
            Cursor cursor1 = database.rawQuery("Select serving From caloriescalculator Where name = '" + returned_meal + "'", null);
            cursor1.moveToFirst();
            while (!cursor1.isAfterLast()) {
                getgm=String.valueOf(cursor1.getFloat(0));
                // String gett=returned_meal;
                cursor1.moveToNext();
            }
            cursor1.close();
        }

       // return getcalories;
       // return getgm;
        String ar[] = new String[2];
        ar[0]= getcalories;
        ar[1] =  getgm;
        return ar;
    }
}