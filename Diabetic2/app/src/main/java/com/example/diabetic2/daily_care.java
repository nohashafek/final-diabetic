package com.example.diabetic2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class daily_care extends AppCompatActivity {
    TextView caretext;
    ImageView careimage;
   int first,second,third,forth,fifth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_care);
        first=R.drawable.examination;
        second=R.drawable.diabeticcream1;
        third=R.drawable.nail;
        forth=R.drawable.socks;
       fifth=R.drawable.warning1;

        caretext = findViewById(R.id.caretxt);
        careimage = findViewById(R.id.careimg);
        final String[] Textlist = {"Have your feet reguralrly examined", "use moisturising oil or cream for dry skin to prevent cracking", "cut your nails by following the shape of the end of your toe", "wear specified diabetic socks with any footwear", "Don't wear tight socks at ankle as this may affects your circulation"};
        final int[] imagelist={first,second,third,forth,fifth};
        Random random = new Random();
        int randomnumber=random.nextInt(Textlist.length);
        String randomText = Textlist[randomnumber];
        caretext.setText(randomText);

        careimage.setImageResource(imagelist[randomnumber]);
    }
}