package com.example.diabetic2;

public class accessmedication {
    String name;
  String concentration;
  String category;

    public accessmedication() {
    }

    public accessmedication( String name, String concentration ,String category) {
        this.name = name;
        this.concentration = concentration;
        this.category=category;
    }

    public String getname() {
        return name;
    }

    public String getconcentration() {
        return concentration;
    }
    public String getcategory() {
        return category;
    }
}
