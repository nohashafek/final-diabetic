package com.example.diabetic2;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayList;

public class medication extends AppCompatActivity {

    DatabaseReference myRef;
    String currentid;
    FirebaseDatabase mFirebaseDatabase;
    private static final String USERS = "Patients";
 public static ArrayList<String>namess=new ArrayList<String>();
    public static ArrayList<String>doses=new ArrayList<String>();
    public static ArrayList<String>categories=new ArrayList<String>();
     ImageButton add;
   public ListView listview;
    ArrayAdapter<String>adapter;
   public String returnedmedication;
    String returneddose;
   EditText searchfor;
    String returnedcategoryagain;

    //addmedication addm=new addmedication();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medication);
       currentid=login.idd;

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference(USERS).child(currentid).child("prescriptions");

        searchfor=findViewById(R.id.search);
     add =findViewById(R.id.addmedication);
     listview =findViewById(R.id.list);
        //String[]names={"(Amaryl","Glucotrol","Micronase","Glynase","Diabinese","Orinase","Tolinase","Starlix"};



    adapter=new ArrayAdapter<String>(medication.this,android.R.layout.simple_list_item_1,namess);
    //ArrayAdapter <ArrayList>adapter=new ArrayAdapter<ArrayList>(this,android.R.layout.simple_list_item_1,names);
    listview.setAdapter(adapter);




        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(medication.this, addmedication.class);
                startActivityForResult(intent,1);

            }
        });

searchfor.addTextChangedListener(new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        medication.this.adapter.getFilter().filter(s);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
});


    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
               returnedmedication = data.getStringExtra("n");
              returneddose = data.getStringExtra("g");
                returnedcategoryagain = data.getStringExtra("c");

                namess.add(returnedmedication);
                doses.add(returneddose);
                categories.add(returnedcategoryagain);
               adapter.notifyDataSetChanged();
                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {


                            Intent intent=new Intent(medication.this, medicationdetails.class);
                            intent.putExtra("namedetail",namess.get(i));
                            intent.putExtra("gmdetail",doses.get(i));
                        intent.putExtra("categorydetail",categories.get(i));

                            startActivity(intent);


                    }
                });

            }
        }
    }

//    @Override
//    public void onBackPressed() {
//        Intent ii=new Intent(medication.this, choose.class);
////
//        // ii.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//        startActivity(ii);
//    }

    @Override
    protected void onStart() {
        super.onStart();
        add =findViewById(R.id.addmedication);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(medication.this, addmedication.class);
                startActivityForResult(intent,1);

            }
        });
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                ArrayList<String>medicationarray=new ArrayList<String>();
//                ArrayList<String>concenttrationarray=new ArrayList<String>();
//                ArrayList<String>categoryarray=new ArrayList<String>();
//                DataPoint[] dp=new DataPoint[(int) dataSnapshot.getChildrenCount()];
                final String[] medicationarray=new String[(int) dataSnapshot.getChildrenCount()];
                final String[] medicationconcentration=new String[(int) dataSnapshot.getChildrenCount()];
                final String[] medicationcategory=new String[(int) dataSnapshot.getChildrenCount()];
                int index=0;
                for (DataSnapshot mydatasnap:dataSnapshot.getChildren())
                {
                    medicationarray[index]= (String) mydatasnap.child("name").getValue();
                    medicationconcentration[index]= (String) mydatasnap.child("concentration").getValue();
                    medicationcategory[index]= (String) mydatasnap.child("category").getValue();
                    index++;
//                    accessmedication info =mydatasnap.getValue(accessmedication.class);
//                    dp[index]=new DataPoint(info.getname(),info.getconcentration(),info.getcategory());
//                    String name=""+ ds.child("name").getValue();

                }
                adapter=new ArrayAdapter<String>(medication.this,android.R.layout.simple_list_item_1,medicationarray);
                //ArrayAdapter <ArrayList>adapter=new ArrayAdapter<ArrayList>(this,android.R.layout.simple_list_item_1,names);
                listview.setAdapter(adapter);
                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int index, long id) {


                        Intent intent=new Intent(medication.this, medicationdetails.class);
                        intent.putExtra("namedetail",medicationarray[index]);
                        intent.putExtra("gmdetail",medicationconcentration[index]);
                        intent.putExtra("categorydetail",medicationcategory[index]);
                        startActivity(intent);


                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

}
