package com.example.diabetic2;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.util.ArrayList;


public class AlertReceiver extends BroadcastReceiver {
    int notificationid = 101;
    String channelid;
    CharSequence channelName;
    int importance;
    Context cxt;
    NotificationManagerCompat manager;
    String datareturnedfinal;

    ArrayList<String> recordednames=new ArrayList<String>();
    @Override
    public void onReceive(Context context, Intent intent) {
        cxt=context;
        datareturnedfinal=intent.getStringExtra("datareturned1");
        Log.d("transmit2", "This "+ datareturnedfinal);
//        SharedPreferences prefs1 = PreferenceManager.getDefaultSharedPreferences(cxt);
//        datareturned1= prefs1.getString("string_id1", "no id");
//        recordednames.add( datareturned1);

//        SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(cxt);
//        datareturned2= prefs2.getString("string_id2", "no id");
          addnotification( datareturnedfinal);


      //medication medicine =new medication();
      // String toastappeared= medicine.returnedmedication;

       // Toast.makeText(context, "take your medicine ", Toast.LENGTH_SHORT).show();

      //Toast.makeText(context, "Take "+toastappeared, Toast.LENGTH_SHORT).show();

     // notify n=new notify();
     // n.notificationmsg();

    }

    public void addnotification(String s){



        for(int i = 0; i< medication.namess.size(); i++) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            channelid = "channel_id";
            channelName = "ch name";
            importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(channelid, channelName, importance);
            notificationChannel.setDescription("This is Channel 1");
            manager= NotificationManagerCompat.from(cxt);

            //  manager = cxt.getSystemService(NotificationManagerCompat.class);
            manager.createNotificationChannel(notificationChannel);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(cxt, channelid);
            builder.setContentTitle("Diabetic Guard alert");
            builder.setContentText("take your medicine("+s+")");
            builder.setSmallIcon(R.drawable.pills_red_blue);
            builder.setLargeIcon(BitmapFactory.decodeResource(cxt.getResources(), R.drawable.pills_red_blue));
            // builder .setStyle(new NotificationCompat.BigPictureStyle()
            //.bigPicture(BitmapFactory.decodeResource(getResources(), R.drawable.alert)));
            builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            builder.setVibrate(new long[]{
                    500,
                    500,
                    500,
                    500
            });
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            // builder.getBigContentView();

           // Intent intent = new Intent(this, MainActivity.class);
            // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            // PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
            // builder.setContentIntent(pendingIntent).setAutoCancel(true);

            manager.notify(notificationid, builder.build());
        }
        else{
            Intent intent = new Intent(cxt, MainActivity.class);
// use System.currentTimeMillis() to have a unique ID for the pending intent
            PendingIntent pIntent = PendingIntent.getActivity(cxt, (int) System.currentTimeMillis(), intent, 0);

            Notification builder=new Notification.Builder(cxt)
          .setContentTitle("Diabetic Guard alert")
        .setContentText("take your medicine("+s+")")
            .setSmallIcon(R.drawable.pills_red_blue)
            .setLargeIcon(BitmapFactory.decodeResource(cxt.getResources(), R.drawable.pills_red_blue))
            // builder .setStyle(new NotificationCompat.BigPictureStyle()
            //.bigPicture(BitmapFactory.decodeResource(getResources(), R.drawable.alert)));
           .setPriority(Notification.PRIORITY_DEFAULT)

          .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .addAction(R.drawable.pills_red_blue, "More", pIntent)
                    .addAction(R.drawable.pills_red_blue, "And more", pIntent).build();
            NotificationManager notificationManager =
                    (NotificationManager) cxt.getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0, builder);


        }
        }


    }
}