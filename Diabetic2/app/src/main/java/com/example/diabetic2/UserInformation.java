package com.example.diabetic2;

public class UserInformation {

    String username,mail,phone,birth;

    public UserInformation(){

    }

    public UserInformation(String username, String mail,  String phone, String birth) {
        this.username = username;
        this.mail = mail;
        this.phone = phone;
        this.birth = birth;

    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

}
