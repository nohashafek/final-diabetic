package com.example.diabetic2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class choose extends AppCompatActivity {

private long backpresstime;
    FirebaseAuth fAuth;
    FirebaseUser user;
    TextView sho;
    FirebaseDatabase mFirebaseDatabase;
    DatabaseReference myRef;
    String main;
    private static final String USERS = "Patients";
    String em,na;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        main=login.idd;

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference(USERS).child(main);

        //        Intent intent = getIntent();
//        em = intent.getStringExtra("email");

//        fAuth=FirebaseAuth.getInstance();
//        user = fAuth.getCurrentUser();



        ImageButton foods=findViewById(R.id.foods);
        ImageButton medication=findViewById(R.id.medication);
        ImageButton foot=findViewById(R.id.foot);
//        ImageButton pressure=findViewById(R.id.bloodpressure);
        ImageButton laboratory=findViewById(R.id.laboratory);
        sho=findViewById(R.id.view);


//


        foods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent log=new Intent(getBaseContext(),food.class);
                startActivity(log);
            }
        });


        medication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent log=new Intent(getBaseContext(),medication.class);
                startActivity(log);
            }
        });

//        pressure.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent log=new Intent(getBaseContext(),heartrate.class);
//                startActivity(log);
//            }
//        });

        laboratory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent log=new Intent(getBaseContext(),laboratory.class);
                startActivity(log);
            }
        });


        foot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent log=new Intent(getBaseContext(),foot.class);
                startActivity(log);
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds:dataSnapshot.getChildren())
                {

                    String name=""+ ds.child("lastglucose").getValue();
                    sho.setText(name);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.main_item,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.profile:
                startActivity(new Intent(choose.this,profile.class));
                return true;
            case R.id.gra:
                startActivity(new Intent(choose.this,linechart.class));
                return true;
            case R.id.user:
                startActivity(new Intent(choose.this,userguide.class));
                return true;
            case R.id.logout:
                startActivity(new Intent(getApplicationContext(),login.class));
                Intent in=new Intent(choose.this,login.class);
                SharedPreferences preferences=getSharedPreferences("checkbox",MODE_PRIVATE);
                SharedPreferences.Editor editor=preferences.edit();
                editor.putString("remember","false");
                editor.apply();
                startActivity(in);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if(backpresstime + 2000 > System.currentTimeMillis()){
//            super.onBackPressed();
            Intent exit=new Intent(Intent.ACTION_MAIN);
            exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            exit.addCategory(Intent.CATEGORY_HOME);
            startActivity(exit);
            finish();
            System.exit(0);
            return;
        }else{
            Toast.makeText(getBaseContext(),"press back again to exit",Toast.LENGTH_SHORT).show();
        }
        backpresstime=System.currentTimeMillis();
    }


    }
