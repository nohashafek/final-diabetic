package com.example.diabetic2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.lang.reflect.Array;
import java.security.PrivateKey;
import java.text.SimpleDateFormat;
import java.util.Date;

public class linechart extends AppCompatActivity {

    EditText y;
    Button inser;
    FirebaseDatabase database;
    DatabaseReference reference;

    //    SimpleDateFormat sdf=new SimpleDateFormat("M/d,h");
    SimpleDateFormat sdf=new SimpleDateFormat("h:m");

    GraphView graphView;
    private static final String TAG = "MyActivity";
    Array val[];
    LineGraphSeries series;
    double x;
    private static final String USERS = "Patients";
    String main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linechart);


        main=login.idd;



        database=FirebaseDatabase.getInstance();
        reference=database.getReference(USERS).child(main).child("medicalinformation").child("glucosse");

//        y=findViewById(R.id.yvalue);
//        inser=findViewById(R.id.insert);

        graphView=findViewById(R.id.graph);

        series=new LineGraphSeries();
        graphView.addSeries(series);

        series.setAnimated(true);
        series.setColor(Color.rgb(255,0,0));
        series.setThickness(3);
        // series.setDrawBackground(true);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(10);


        long zee=new Date().getTime();


        graphView.getViewport().setScalable(true);
        graphView.getViewport().setXAxisBoundsManual(true);

        graphView.getViewport().setScalableY(true);
        graphView.getViewport().setYAxisBoundsManual(true);
        graphView.getViewport().setMinY(60);
        graphView.getViewport().setMaxY(400);



//        setListener();
        graphView.getGridLabelRenderer().setNumHorizontalLabels(5);

        graphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(){
            @Override
            public String formatLabel(double value, boolean isValueX) {

                if (isValueX){
//                    Log.i(TAG, "get item number " + value);
                    return "Day" + "\n" + sdf.format(new Date((long) value ));

                }else {
                    Log.i(TAG, "get item numberx " + value);
                    return "GL" + super.formatLabel(value, isValueX);

                }

            }
        });

    }

//    private void setListener() {
//
//        inser.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    String id=reference.push().getKey();
//                    long xn=new Date().getTime();
//                    graphView.getViewport().setXAxisBoundsManual(true);
//                    graphView.getViewport().setMaxX(xn);
//
//
//                    int yn= Integer.parseInt(y.getText().toString());
//                    graphView.getViewport().setYAxisBoundsManual(true);
//                    graphView.getViewport().setMaxY(yn);
//
//                    line point=new line(xn,yn);
////                    int date=new Date().getDay();
////                   String date2=Integer.toString(date);
//
//                    reference.child(id).setValue(point);
//
//                }
//                catch (Exception e){
//                    Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
//                }
//
//
//            }
//        });
//    }

    @Override
    protected void onStart() {
        super.onStart();

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DataPoint[] dp=new DataPoint[(int) dataSnapshot.getChildrenCount()];
                int index=0;
                for (DataSnapshot mydatasnap : dataSnapshot.getChildren()){
                    line point=mydatasnap.getValue(line.class);

                    dp[index]=new DataPoint(point.getxValue(),point.getyVlue());

                    Log.i(TAG, "MyClass.getView() — get item number " + dp[index]);
                   if(index==0) {
                       x=point.getxValue();
//                       y=point.getyVlue();


                       graphView.getViewport().setMinX(x);

                   }
                    graphView.getViewport().setMaxX(point.getxValue());


//                    DataPoint  dataPoint = dp[0];]= dp[0];
//                    if (index == 0){
//                        [long x/int y]= dp[0];
//                        long xn= dataPoint;
//                        graphView.getViewport().setXAxisBoundsManual(true);
//                        graphView.getViewport().setMinX( dataPoint);
//                    }

                    index++;

                }



                series.resetData(dp);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}