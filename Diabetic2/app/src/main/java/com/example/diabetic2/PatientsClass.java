package com.example.diabetic2;

public class PatientsClass {
    String username,mail,phone,birth,gender,typew;

    public PatientsClass(String username, String mail, String phone, String birth,String gender,String typew) {
        this.username = username;
        this.mail = mail;
        this.phone = phone;
        this.birth = birth;
        this.gender = gender;
        this.typew = typew;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTypew() {
        return typew;
    }

    public void setTypew(String typew) {
        this.typew= typew;
    }
}
