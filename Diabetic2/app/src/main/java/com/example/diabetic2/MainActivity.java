package com.example.diabetic2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Handler mHandler = new Handler();
    TextView na;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        na=findViewById(R.id.name);
        String text="Diabetic Guard";

        SpannableString ss= new SpannableString(text);
        StyleSpan bold=new StyleSpan(Typeface.BOLD);
        StrikethroughSpan strikethroughSpan=new StrikethroughSpan();

        ss.setSpan(bold,0,14, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        na.setText(ss);


        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {


                Intent intent=new Intent(MainActivity.this,login.class);
                startActivity(intent);
                finish();
            }
        },2000);
    }
}
