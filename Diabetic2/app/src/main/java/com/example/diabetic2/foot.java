package com.example.diabetic2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationManagerCompat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class foot extends AppCompatActivity {

    private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference reff = firebaseDatabase.getReference();
    private DatabaseReference pressure1 = reff.child("firstpressure");
    private DatabaseReference pressure2 = reff.child("secondpressure");
    private DatabaseReference pressure3 = reff.child("thirdpressure");
    private DatabaseReference pressure4 = reff.child("forthpressure");
    private DatabaseReference pressure5 = reff.child("fifthpressure");
    private DatabaseReference temperature = reff.child("temp");

    public int pre1, pre2, pre3, pre4, pre5, temp;
    TextView temptxt, pressurtxt1, pressurtxt2, pressurtxt3, pressurtxt4, pressurtxt5;
    ImageView p1color, p2color, p3color, p4color, p5color;
    Button green, yellow, care;

    int notificationid = 101;
    String channelid;
    CharSequence channelName;
    int importance;
    Context cxt;
    NotificationManagerCompat manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foot);
        care = findViewById(R.id.care);
//        green=findViewById(R.id.green);
//        yellow=findViewById(R.id. yellow);

//        pressurtxt1 = findViewById(R.id.pressuretxt1);
//        pressurtxt2 = findViewById(R.id.pressuretxt2);
//        pressurtxt3 = findViewById(R.id.pressuretxt3);
//        pressurtxt4 = findViewById(R.id.pressuretxt4);
//        pressurtxt5 = findViewById(R.id.pressuretxt5);
        temptxt = findViewById(R.id.t1);


        p1color = findViewById(R.id.p1color);
        p2color = findViewById(R.id.p2color);
        p3color = findViewById(R.id.p3color);
        p4color = findViewById(R.id.p4color);
        p5color = findViewById(R.id.p5color);


        care.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(foot.this, daily_care.class);
                startActivity(intent);
            }
        });

//        green.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//                int gree= Color.parseColor("#1BEE30");
//                p1color.setColorFilter(gree);
//            }
//        });
//
//        yellow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                int gree= Color.parseColor("#F0AE09");
//                p1color.setColorFilter(gree);
//            }
//        });
    }

    protected void onStart() {
        super.onStart();
        temperature.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String messaget = dataSnapshot.getValue(String.class);
                temp = Integer.parseInt(messaget);

                temptxt.setText("temperature=" + messaget);
                if (temp == 32) {

                    Toast.makeText(getApplicationContext(), "High Temperature ",
                            Toast.LENGTH_LONG).show();


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        pressure1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String message1 = dataSnapshot.getValue(String.class);
                pre1 = Integer.parseInt(message1);

//                pressurtxt1.setText("pressure1=" + message1);

                if (pre1 <= 10) {
                    int value = Color.parseColor("#AAA8A9");
                    p1color.setColorFilter(value);


                } else if (pre1 <= 80 && pre1 > 11) {
                    int value = Color.parseColor("#335bff");
                    p1color.setColorFilter(value);
                } else if (pre1 <= 150 && pre1 > 81) {
                    int value = Color.parseColor("#fe7b07");
                    p1color.setColorFilter(value);

                } else {
                    Toast.makeText(getApplicationContext(), "High Pressure ",
                            Toast.LENGTH_SHORT).show();
                    int value = Color.parseColor("#f51506");
                    p1color.setColorFilter(value);
                    // addnotification();


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        pressure2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String message2 = dataSnapshot.getValue(String.class);
                pre2 = Integer.parseInt(message2);

//                pressurtxt2.setText("pressure2=" + message2);

                if (pre2 <= 10) {
                    int value = Color.parseColor("#AAA8A9");
                    p2color.setColorFilter(value);


                } else if (pre2 <= 80 && pre2 > 11) {
                    int value = Color.parseColor("#335bff");
                    p2color.setColorFilter(value);
                } else if (pre2 <= 150 && pre2 > 81) {
                    int value = Color.parseColor("#fe7b07");
                    p2color.setColorFilter(value);

                } else {
                    Toast.makeText(getApplicationContext(), "High Pressure ",
                            Toast.LENGTH_SHORT).show();
                    int value = Color.parseColor("#f51506");
                    p2color.setColorFilter(value);
                    //addnotification();


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        pressure3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String message3 = dataSnapshot.getValue(String.class);
                pre3 = Integer.parseInt(message3);

//                pressurtxt3.setText("pressure3=" + message3);

                if (pre3 <= 10) {
                    int value = Color.parseColor("#AAA8A9");
                    p3color.setColorFilter(value);


                } else if (pre3 <= 80 && pre3 > 11) {
                    int value = Color.parseColor("#335bff");
                    p3color.setColorFilter(value);
                } else if (pre3 <= 150 && pre3 > 81) {
                    int value = Color.parseColor("#fe7b07");
                    p3color.setColorFilter(value);

                } else {
                    Toast.makeText(getApplicationContext(), "High Pressure ",
                            Toast.LENGTH_SHORT).show();
                    int value = Color.parseColor("#f51506");
                    p3color.setColorFilter(value);
                    //addnotification();


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        pressure4.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String message4 = dataSnapshot.getValue(String.class);
                pre4 = Integer.parseInt(message4);

//                pressurtxt4.setText("pressure4=" + message4);
                if (pre4 <= 10) {
                    int value = Color.parseColor("#AAA8A9");
                    p4color.setColorFilter(value);


                } else if (pre4 <= 80 && pre4 > 11) {
                    int value = Color.parseColor("#335bff");
                    p4color.setColorFilter(value);
                } else if (pre4 <= 150 && pre4 > 81) {
                    int value = Color.parseColor("#fe7b07");
                    p4color.setColorFilter(value);

                } else {
                    Toast.makeText(getApplicationContext(), "High Pressure ",
                            Toast.LENGTH_SHORT).show();
                    int value = Color.parseColor("#f51506");
                    p4color.setColorFilter(value);
                    //addnotification();


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        pressure5.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String message5 = dataSnapshot.getValue(String.class);
                pre5 = Integer.parseInt(message5);

//                pressurtxt5.setText("pressure5=" + message5);

                if (pre5 <= 10) {
                    int value = Color.parseColor("#AAA8A9");
                    p5color.setColorFilter(value);


                } else if (pre5 <= 80 && pre5 > 11) {
                    int value = Color.parseColor("#335bff");
                    p5color.setColorFilter(value);
                } else if (pre5 <= 150 && pre5 > 81) {
                    int value = Color.parseColor("#fe7b07");
                    p5color.setColorFilter(value);

                } else {
                    Toast.makeText(getApplicationContext(), "High Pressure ",
                            Toast.LENGTH_SHORT).show();
                    int value = Color.parseColor("#f51506");
                    p5color.setColorFilter(value);
                    //addnotification();


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}