package com.example.diabetic2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class food extends AppCompatActivity {
    Button ask,takepicture;
    Button barchart, pichart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);


        ask=findViewById(R.id.Ask);
        takepicture=findViewById(R.id.takephoto);

//        barchart=findViewById(R.id.bar);
//        pichart=findViewById(R.id.pi);

        ask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent log=new Intent(getBaseContext(),ask_without_picture.class);
                startActivity(log);
            }
        });

        takepicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent log=new Intent(getBaseContext(),takepicture.class);
                startActivity(log);
            }
        });
//
//        barchart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent log=new Intent(getBaseContext(),barchat.class);
//                startActivity(log);
//            }
//        });
//
//        pichart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent log=new Intent(getBaseContext(),pichart.class);
//                startActivity(log);
//            }
//        });
    }
}
