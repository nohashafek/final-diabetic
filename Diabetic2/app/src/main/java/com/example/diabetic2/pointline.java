package com.example.diabetic2;

public class pointline {
    long xValue;
    int yVlue;

    public pointline() {
    }

    public pointline(long xValue, int yVlue) {
        this.xValue = xValue;
        this.yVlue = yVlue;
    }

    public long getxValue() {
        return xValue;
    }

    public int getyVlue() {
        return yVlue;
    }
}
