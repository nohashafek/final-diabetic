package com.example.diabetic2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class createaccount extends AppCompatActivity {
    String emailtext;
    String usernametext;
    String passwordtext;
    String phonetext;
    String birthtext;
    DatabaseHelper myDb;
    EditText sname,semail,spass,sphone,sbirth;
    Button sregister,trr;
    ProgressBar progressBar;
    FirebaseAuth fAuth;
    CheckBox sshow;
    FirebaseDatabase rootnode;
    DatabaseReference reference;
    PatientsClass patientsClass;
    String record="null";
    Spinner diabetictype;
    String list[] = {"Type","Type I" , "Type II"};
    ArrayAdapter<String> dataadapter;
    RadioGroup radioGroup;
    RadioButton radioButton;
    String gender="null";
    String birth="null";
    String username="null";
    String phone="null";
    String email="null";
    String password="null";
    String typew="null";
    public static  String id;
    private static final String USERS = "Patients";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createaccount);

        myDb = new DatabaseHelper(this);

        sname=findViewById(R.id.susername);
        semail=findViewById(R.id.semail);
        spass=findViewById(R.id.spassword);
        sphone=findViewById(R.id.sosphone);
        sbirth=findViewById(R.id.sbirthday);
        sregister=findViewById(R.id.sregister);
        progressBar=findViewById(R.id.progressbar);
        sshow=findViewById(R.id.sshowpass);

        // //radio button
        radioGroup=findViewById(R.id.check);

        // //Spinener
        diabetictype=findViewById(R.id.stype);
        dataadapter =new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,list);
        diabetictype.setAdapter(dataadapter);

        diabetictype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        record = "Type";
                        break;
                    case 1:
                        record = "Type I";
                        break;
                    case 2:
                        record = "Type II";
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        rootnode = FirebaseDatabase.getInstance();
        reference = rootnode.getReference(USERS);
        fAuth= FirebaseAuth.getInstance();

        sshow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    spass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else {
                    spass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });





        sregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                dgg=0;
                boolean isInserted = myDb.insertData(emailtext,
                        usernametext,
                        passwordtext, phonetext,  birthtext);
                if(isInserted == true)
                    Toast.makeText(createaccount.this,"Data Inserted",Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(createaccount.this,"Data not Inserted",Toast.LENGTH_LONG).show();


                int radioid = radioGroup.getCheckedRadioButtonId();
                findradio(radioid);

                typew = record.trim();
                email = semail.getText().toString().trim();
                password = spass.getText().toString().trim();
                phone = sphone.getText().toString().trim();
                birth = sbirth.getText().toString().trim();
//                dgg=Integer.parseInt(birth);
                username = sname.getText().toString().trim();




                if (TextUtils.isEmpty(email)) {
                    semail.setError("email is required");
                    return;
                }


                if (TextUtils.isEmpty(password)) {
                    spass.setError("password is required");
                    return;
                }

                if (password.length() < 2) {

                    spass.setError("too short password");
                    return;
                }
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {

                    Toast.makeText(createaccount.this,"Please enter a valid email address",Toast.LENGTH_LONG).show();
                    return;
                }

//                if (100 < dgg && dgg< 0) {
//                    Toast.makeText(createaccount.this,"Please check your age",Toast.LENGTH_LONG).show();
//                    return;
//                }
                if (gender=="null") {

                    Toast.makeText(createaccount.this,"Please enter your gender",Toast.LENGTH_LONG).show();
                    return;
                }
                if (typew=="Type") {

                    Toast.makeText(createaccount.this,"Please enter your type",Toast.LENGTH_LONG).show();
                    return;
                }



                //                if(phone.length() != 11){
                //                    spass.setError("wrong phone");
                //                    return;
                //                }

                patientsClass = new PatientsClass(username, email, phone, birth, gender, typew);
                registerUser();
                progressBar.setVisibility(View.VISIBLE);

            }
        });
    }


    public void registerUser() {
        fAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
//                    Toast.makeText(createaccount.this, "user created", Toast.LENGTH_LONG).show();
                    FirebaseUser user = fAuth.getCurrentUser();
                    id=user.getUid();
                    reference.child(id).child("profile").setValue(patientsClass);
                    reference.child(id).child("profile").child("id").setValue(id);
//                    updateUI(user);

                    startActivity(new Intent(getApplicationContext(), login.class));
                } else {
                    Toast.makeText(createaccount.this, "error !" + task.getException().getMessage(), Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void findradio(int radioid) {
        switch (radioid){
            case R.id.malegender:
                gender="male";
//                Toast.makeText(createaccount.this,gender,Toast.LENGTH_LONG).show();
                break;
            case R.id.femalegender:
                gender="female";
//                Toast.makeText(createaccount.this,"female",Toast.LENGTH_LONG).show();
                break;

        }
    }

//    public Boolean checkno(String sn){
//        Boolean check=false;
//        String no="\\d*\\.?\\d+";
//        CharSequence input=sn;
//        Pattern pt=Pattern.compile(no,Pattern.CASE_INSENSITIVE);
//        Matcher matcher=pt.matcher(input);
//        if (matcher.matches()){
//            check=true;
//        }
//        return check;
//    }


}