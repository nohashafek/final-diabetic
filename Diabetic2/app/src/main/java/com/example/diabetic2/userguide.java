package com.example.diabetic2;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

public class userguide extends AppCompatActivity {
    Button playvideo;
    VideoView video;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userguide);
        video=findViewById(R.id.video);
        playvideo=findViewById(R.id.play);



    }

    public void btn(View view) {
        MediaController m = new MediaController(this);
        video.setMediaController(m);

        String path ="android.resource://com.example.diabetic2/"+R.raw.guide;

        Uri u= Uri.parse(path);


        video.setVideoURI(u);

        video.start();
    }
}
