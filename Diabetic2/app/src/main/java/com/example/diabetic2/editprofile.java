package com.example.diabetic2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class editprofile extends AppCompatActivity {

    EditText edname, edphone, edbirth;
    Button ediiit;
    Spinner diabetictype;
    RadioGroup radioGroup;

    String fname,fbirth,fphone;

    String record = "null";
    String list[] = {"Type", "Type I", "Type II"};
    ArrayAdapter<String> dataadapter;

    String gender = "null";
    String birth = "null";
    String username = "null";
    String phone = "null";
    String typew = "null";

    String main;
    private static final String USERS = "Patients";
    FirebaseDatabase rootnode;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);

        main=login.idd;
        rootnode = FirebaseDatabase.getInstance();
        reference = rootnode.getReference(USERS).child(main);

        edname = findViewById(R.id.editrname);
        edphone = findViewById(R.id.editphone);
        edbirth = findViewById(R.id.editbirthday);
        ediiit = findViewById(R.id.editregister);
        // //radio button
        radioGroup = findViewById(R.id.edcheck);
        // //Spinener
        diabetictype = findViewById(R.id.edittype);


        dataadapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);
        diabetictype.setAdapter(dataadapter);
        diabetictype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        record = "Type";
                        break;
                    case 1:
                        record = "Type I";
                        break;
                    case 2:
                        record = "Type II";
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ediiit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int radioid = radioGroup.getCheckedRadioButtonId();
                findradio(radioid);

                typew = record.trim();
                phone = edphone.getText().toString().trim();
                birth = edbirth.getText().toString().trim();
                username = edname.getText().toString().trim();




                if (username != "null") {

                    reference.child("profile").child("username").setValue(username);
                }
                if(TextUtils.isEmpty(username)){
                    reference.child("profile").child("username").setValue(fname);
                }


                if (birth != "null") {

                    reference.child("profile").child("birth").setValue(birth);
                }
                if (TextUtils.isEmpty(birth))  {
                    reference.child("profile").child("birth").setValue(fbirth);
                }




                if (phone != "null") {

                    reference.child("profile").child("phone").setValue(phone);
                }
                if (TextUtils.isEmpty(phone)){
                    reference.child("profile").child("phone").setValue(fphone);
                }



                if (typew != "Type") {

                    reference.child("profile").child("typew").setValue(typew);
                }

                if (gender != "null") {

                    reference.child("profile").child("gender").setValue(gender);
                }

                Toast.makeText(editprofile.this,"Data has been changed",Toast.LENGTH_LONG).show();
                startActivity(new Intent(getApplicationContext(), choose.class));

            }
        });
    }


    private void findradio(int radioid) {
        switch (radioid) {
            case R.id.malegender:
                gender = "male";
//                Toast.makeText(editprofile.this,gender,Toast.LENGTH_LONG).show();
                break;
            case R.id.femalegender:
                gender = "female";
                break;

        }
    }


    @Override
    public void onStart() {
        super.onStart();
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds:dataSnapshot.getChildren())
                {

                    fname=""+ ds.child("username").getValue();
                    fphone=""+ ds.child("phone").getValue();
                    fbirth=""+ ds.child("birth").getValue();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}